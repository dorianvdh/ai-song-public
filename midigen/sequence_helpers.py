import os

import copy
import numpy as np

import magenta.music as mm
from magenta.music.midi_io import note_sequence_to_midi_file, note_sequence_to_pretty_midi, midi_to_note_sequence, midi_file_to_note_sequence
from magenta.music.chord_inference import infer_chords_for_sequence
from magenta.music.chords_lib import BasicChordRenderer
from magenta.music.protobuf.music_pb2 import NoteSequence

from magenta.music.sequences_lib import concatenate_sequences, repeat_sequence_to_duration, apply_sustain_control_changes
from magenta.music.sequences_lib import extract_subsequence, quantize_note_sequence, infer_dense_chords_for_sequence
from magenta.music.sequences_lib import split_note_sequence


WORKING_DIR = os.path.abspath(__file__).replace('midigen/sequence_helpers.py', '')
HOME_DIR = os.path.abspath(__file__).replace('midigen/sequence_helpers.py', '')


def plot_n_play_seq(ns):
    mm.plot_sequence(ns)
    mm.play_sequence(ns, synth=mm.fluidsynth)



def init_note_sequence(primer_midi=None, primer_notes=None, primer_melody=None,
                       drums=False, qpm=None, steps_pq = mm.DEFAULT_STEPS_PER_QUARTER):
    """
    Args:
        -qpm: force it (convert it worst case) to be this qpm

    TODO: rewrite it looks ugly af
    """
    if primer_notes and not qpm:
        raise TypeError('You need to specify a QPM when setting the qpm')
    if primer_midi:
        ns = midi_file_to_note_sequence(primer_midi)
        if qpm:
            primer_melody = NoteSequence()
            qpm_ns = ns.tempos.pop().qpm
            primer_melody.tempos.add(qpm=qpm)
            conv = qpm_ns / qpm
            print(conv)

            for note in ns.notes:
                primer_melody.notes.add(pitch=note.pitch, start_time=note.start_time*conv,
                                        end_time=note.end_time*conv, velocity=note.velocity)

            primer_melody.total_time = max(n.end_time for n in primer_melody.notes)
            primer_melody.ticks_per_quarter = ns.ticks_per_quarter

            print('Changed tempo from {} to {}'.format(qpm_ns, qpm))
            return primer_melody
        else:
            return ns

    elif primer_notes:
        if drums:
            primer_drums = mm.DrumTrack([frozenset(t) for t in primer_notes])
            return primer_drums.to_sequence(qpm=qpm)
        else:
            sec_ps = 60 / qpm / steps_pq
            primer_melody = NoteSequence()
            for pitch, start, duration in primer_notes:
                primer_melody.notes.add(pitch=pitch, start_time=start, end_time=duration * sec_ps, velocity=80)
            primer_melody.tempos.add(qpm=qpm)
            primer_melody.total_time = max(n.end_time for n in primer_melody.notes)
            return primer_melody

    else:
        return NoteSequence()


def change_tempo(ns, qpm):
    qpm_old = ns.tempos.pop().qpm
    conversion = qpm_old / qpm
    ns.tempos.add(qpm=qpm)

    for note in ns.notes:
        note.start_time = conversion * note.start_time
        note.end_time = conversion * note.end_time

    ns.total_time = conversion * ns.total_time

    return ns


def extract_drums(seq):
    midi = note_sequence_to_pretty_midi(seq)
    midi.instruments = [instr for instr in midi.instruments if instr.is_drum]
    return midi_to_note_sequence(midi)


def extract_melody(seq, program):
    midi = note_sequence_to_pretty_midi(seq)
    midi.instruments = [instr for instr in midi.instruments if instr.program == program]
    return midi_to_note_sequence(midi)


def replace_drums(seq, new_drums_seq):
    midi = note_sequence_to_pretty_midi(seq)
    midi_drums = note_sequence_to_pretty_midi(new_drums_seq)
    midi.instruments = [instr for instr in midi.instruments if not instr.is_drum] + midi_drums.instruments
    return midi_to_note_sequence(midi)


def humanize_drums(seq, drum_humanizer, n_bars=4, layer=False):
    """Also layering"""
    tempo = seq.tempos[0].qpm
    bar_duration = (60 / tempo) * 4

    n_bars = min(n_bars, int(np.round(seq.total_time) / bar_duration))
    n_repeats = n_bars // 2
    seqs_2bar = split_note_sequence(extract_drums(seq), 2 * bar_duration)[:n_repeats]

    z_drum, mu, sigma = drum_humanizer.encode(seqs_2bar)
    drum_human = drum_humanizer.decode(z=z_drum, length=32)
    if not layer:
        for drum in drum_human:
            drum.total_time = 2 * bar_duration

    new_drums = concatenate_sequences(drum_human)
    if layer:
        new_drums = repeat_sequence_to_duration(new_drums, n_repeats * bar_duration)
    return replace_drums(seq, new_drums)


def add_chords(seq, instrument=1, program=56, velocity=38):
    quant = quantize_note_sequence(seq, steps_per_quarter=4)
    infer_chords_for_sequence(quant)
    BasicChordRenderer(instrument=1, program=56, velocity=38).render(quant)
    # undoing the quantization
    seq = midi_to_note_sequence(note_sequence_to_pretty_midi(quant))
    return seq


def vocalize_melody_sequence(seq, seconds_per_step=0.125,
                             min_steps=2, drop_ratio=0.25,
                             program=0, vocal_program=54,
                             velocity=100, **kwargs):
    voc = NoteSequence()
    voc.total_time = seq.total_time

    def decide_to_keep_note():
        return bool(np.random.choice([0, 1], p=[drop_ratio, 1 - drop_ratio]))

    program_notes = sorted([n for n in seq.notes if n.program == program and not n.is_drum and decide_to_keep_note()],
                           key=lambda n: n.start_time)
    for i in np.arange(0, len(program_notes) - 1, 2):
        n1, n2 = copy.deepcopy(program_notes[i]), copy.deepcopy(program_notes[i + 1])

        if (n1.end_time - n1.start_time < (min_steps) * seconds_per_step
                or n2.end_time - n2.start_time < (min_steps) * seconds_per_step):
            ### merge the notes
            pitch = n1.pitch
            start_time = n1.start_time
            end_time = n2.end_time
            voc.notes.append(NoteSequence.Note(pitch=pitch, start_time=start_time, end_time=end_time,
                                               program=vocal_program, velocity=velocity, **kwargs))

        else:
            voc.notes.append(NoteSequence.Note(pitch=n1.pitch, start_time=n1.start_time, end_time=n1.end_time,
                                               program=vocal_program, velocity=velocity, **kwargs))
            voc.notes.append(NoteSequence.Note(pitch=n2.pitch, start_time=n2.start_time, end_time=n2.end_time,
                                               program=vocal_program, velocity=velocity, **kwargs))
    return voc


def add_vocals(seq, program=0, vocal_program=54, velocity=100, **kwargs):
    voc_seq = vocalize_melody_sequence(seq, program=program, vocal_program=vocal_program, velocity=velocity, **kwargs)
    seq.notes.extend(voc_seq.notes)
    return seq


def change_instruments(seq, ins_dict):
    midi = note_sequence_to_pretty_midi(seq)
    for i in midi.instruments:
        if not i.is_drum:
            i.program = ins_dict.get(i.program, i.program)
    return midi_to_note_sequence(midi)


def drop_melody(seq, program):
    new_notes = []
    end_time = seq.total_time
    for note in seq.notes:
        if note.program != program:
            new_notes.append(note)
    del seq.notes[:]
    seq.notes.extend(new_notes)
    return seq