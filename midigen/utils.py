import os
import logging

from functools import wraps

COUNTER = 0

def create_dir_if_not_exists(path):
    if not os.path.exists(path):
        os.makedirs(path)


def pxtry(func):

    @wraps(func)
    def function_wrapper(x, *args, **kwargs):
        global COUNTER
        logger = logging.getLogger(__name__)
        logger.setLevel(logging.INFO)
        COUNTER +=1
        logger.info('{}: {}'.format(COUNTER, x))
        try:
            func(x, *args, **kwargs)
            return True, x
        except Exception as e:
            logger.warning('Error for {}: {}'.format(x, type(e)))
            return False, x

    return function_wrapper


