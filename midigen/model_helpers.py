import os

import tensorflow as tf
from six.moves import urllib

from functools import reduce
import subprocess
import shutil

from magenta.models.music_vae import TrainedModel, configs
from magenta.models.shared.sequence_generator_bundle import read_bundle_file

import magenta.music as mm
from magenta.music.midi_io import midi_file_to_note_sequence, note_sequence_to_midi_file
from magenta.music.midi_io import note_sequence_to_pretty_midi
from magenta.music.protobuf.generator_pb2 import GeneratorOptions
from magenta.models.music_vae.configs import CONFIG_MAP, Config, merge_hparams
from magenta.models.music_vae import MusicVAE
from magenta.models.music_vae.data import TrioConverter
from tensorflow.contrib.training import HParams
from magenta.models.music_vae import lstm_models
from magenta.pipelines.pipeline import tf_record_iterator
from magenta.music.protobuf.music_pb2 import NoteSequence

from pretty_midi import PrettyMIDI

from .sequence_helpers import change_tempo



def create_generator(bundle_path, seq_gen, gen_id):
    bundle = read_bundle_file(bundle_path)
    generator_map = seq_gen.get_generator_map()
    generator = generator_map[gen_id](checkpoint=None, bundle=bundle)
    return generator


def gen_options(temperature=1.1, start_time=0, end_time=8,
                beam_size=1, branch_factor=1, steps_per_iteration=1,
                condition_on_primer=False, inject_primer_during_generation=False,
                pitch_class_histogram=None):
    """

    :param temperature:
    :param start_time:
    :param end_time:
    :param beam_size:
    :param branch_factor:
    :param steps_per_iteration:
    :param condition_on_primer:
    :return:
    """
    options = GeneratorOptions()
    options.args['temperature'].float_value = temperature
    options.args['beam_size'].int_value = beam_size
    options.args['branch_factor'].int_value = branch_factor
    options.args['steps_per_iteration'].int_value = steps_per_iteration
    options.args['condition_on_primer'].bool_value = condition_on_primer
    options.args['no_inject_primer_during_generation'].bool_value = not inject_primer_during_generation
    if pitch_class_histogram:
        options.args['pitch_class_histogram'].string_value = (
            str(pitch_class_histogram))
    options.generate_sections.add(
        start_time=start_time,
        end_time=end_time
    )
    return options


def download_checkpoint(model_name, checkpoint_name, target_dir):
    """
    Downloads a Magenta checkpoint to target directory.

    Target directory target_dir will be created if it does not already exist.

      :param model_name: magenta model name to download
      :param checkpoint_name: magenta checkpoint name to download
      :param target_dir: local directory in which to write the checkpoint
    """
    tf.gfile.MakeDirs(target_dir)
    checkpoint_target = os.path.join(target_dir, checkpoint_name)
    if not os.path.exists(checkpoint_target):
        response = urllib.request.urlopen(
            f"https://storage.googleapis.com/magentadata/models/"
            f"{model_name}/checkpoints/{checkpoint_name}")
        data = response.read()
        local_file = open(checkpoint_target, 'wb')
        local_file.write(data)
        local_file.close()


def get_model(name, target_dir='checkpoints', batch_size=8, **kwargs):
    """
    Returns the model instance from its name.

      :param name: the model name
    """
    checkpoint = name + ".tar"
    download_checkpoint("music_vae", checkpoint, target_dir)
    return TrainedModel(
        # Removes the .lohl in some training checkpoint which shares the same config
        configs.CONFIG_MAP[name.split(".")[0] if "." in name else name],
        checkpoint_dir_or_path=os.path.join(target_dir, checkpoint),
        batch_size=batch_size,
        **kwargs)


def sample(model_name, n_samples, bars_per_sample=2, qpm=mm.DEFAULT_QUARTERS_PER_MINUTE, **kwargs):
    model = get_model(model_name)
    sec_per_step = (60 / qpm / mm.DEFAULT_STEPS_PER_QUARTER)
    sec_per_bar = sec_per_step * mm.DEFAULT_STEPS_PER_BAR
    seqs = model.sample(n_samples, length=bars_per_sample * mm.DEFAULT_STEPS_PER_BAR, **kwargs)
    sample_length = (60 / qpm / mm.DEFAULT_STEPS_PER_QUARTER) * sec_per_bar * bars_per_sample
    for seq in seqs:
        change_tempo(seq, qpm)
        seq.total_time = sample_length
    return seqs

def sample_120(model_name, n_samples, bars_per_sample=2, **kwargs):
    qpm = 120
    model = get_model(model_name)
    seqs = model.sample(n_samples, length=bars_per_sample * mm.DEFAULT_STEPS_PER_BAR, **kwargs)
    sample_length = (60 / qpm / mm.DEFAULT_STEPS_PER_QUARTER) * mm.DEFAULT_STEPS_PER_BAR * bars_per_sample
    for seq in seqs:
        seq.total_time = sample_length
    return seqs

def interpolate(model_name, seq1, seq2, num_steps, length, **kwargs):
    model = get_model(model_name)
    ipseqs = model.interpolate(seq1, seq2, num_steps, length=length, **kwargs)
    seq = mm.sequences_lib.concatenate_sequences(ipseqs)
    return seq


def groove(model_name, seqs, bars_per_sample):
    sec_per_step = 60 / mm.DEFAULT_QUARTERS_PER_MINUTE / mm.DEFAULT_STEPS_PER_QUARTER
    sec_per_bar = mm.DEFAULT_STEPS_PER_BAR * sec_per_step
    model = get_model(model_name)
    encoding, mu, sigma = model.encode(seqs)
    grooved_seqs = model.decode(z=encoding, length=bars_per_sample*mm.DEFAULT_STEPS_PER_BAR)
    groovy = mm.sequences_lib.concatenate_sequences(grooved_seqs, [bars_per_sample*sec_per_bar] * len(grooved_seqs))
    return groovy


def gen_song_separate_tracks(generators, options, primers, save_path):
    """s
    Args:
        primers: NoteSequence
    """
    tracks = []
    for gen, opt, seq in zip(generators, options, primers):
        gen_seq = gen.generate(seq, opt)
        tracks.append(note_sequence_to_pretty_midi(gen_seq))

    song = PrettyMIDI()
    song.instruments = reduce(lambda x, y: x + y, [t.instruments for t in tracks])
    song.write(save_path)

    return midi_file_to_note_sequence(save_path)


def update_trio_config(name, **kwargs):
    CONFIG_MAP[name] = Config(
        model=MusicVAE(
            lstm_models.BidirectionalLstmEncoder(),
            lstm_models.HierarchicalLstmDecoder(
                lstm_models.SplitMultiOutLstmDecoder(
                    core_decoders=[
                        lstm_models.CategoricalLstmDecoder(),
                        lstm_models.CategoricalLstmDecoder(),
                        lstm_models.CategoricalLstmDecoder()],
                    output_depths=[
                        90,  # melody
                        90,  # bass
                        512,  # drums
                    ]),
                level_lengths=[16, 16],
                disable_autoregression=True)),
        hparams=merge_hparams(
            lstm_models.get_default_hparams(),
            HParams(
                batch_size=256,
                max_seq_len=256,
                z_size=512,
                enc_rnn_size=[2048, 2048],
                dec_rnn_size=[1024, 1024],
                free_bits=256,
                max_beta=0.2,
            )),
        note_sequence_augmenter=None,
        data_converter=TrioConverter(**kwargs),
        train_examples_path=None,
        eval_examples_path=None,
    )


def read_tfrecord(pth):
    iterator = tf_record_iterator(pth, NoteSequence)
    noteseqs = [ns for ns in iterator]
    return noteseqs


def gansynth(midi_pth, ckpt_dir, output_dir, name='gen'):
    process = subprocess.Popen(["gansynth_generate",
                                    "--ckpt_dir={}".format(ckpt_dir),
                                    "--output_dir={}".format(output_dir),
                                    "--midi_file={}".format(midi_pth)])
    stdout = process.communicate()[0]
    respth = os.path.join(output_dir, 'generated_clip.wav')
    newpth = os.path.join(output_dir, '{}.wav'.format(name))
    shutil.move(respth, newpth)
    print("written to {}".format(newpth))
    return stdout