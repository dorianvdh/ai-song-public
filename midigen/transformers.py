from magenta.pipelines.note_sequence_pipelines import *
from magenta.pipelines.pipeline import Pipeline

from magenta.models.music_vae.configs import CONFIG_MAP

import magenta.music as mm
import numpy as np

from magenta.music.sequences_lib import extract_subsequence, quantize_note_sequence
from magenta.music.sequences_lib import repeat_sequence_to_duration
from magenta.music.melodies_lib import Melody
from magenta.music.drums_lib import DrumTrack
from magenta.music.midi_io import note_sequence_to_pretty_midi, midi_to_note_sequence
from pretty_midi import PrettyMIDI
from functools import reduce

from .model_helpers import get_model

QPM = mm.DEFAULT_QUARTERS_PER_MINUTE


class SubSequenceExtractor(NoteSequencePipeline):
    """
    NoteSequencePipeline object that extracts subsequences from a NoteSequence that is too long too process


    Attributes
    ----------
    _max_duration : int
        Maximum allowed duration in seconds of a NoteSequence
    _n_bars : str
        the name of the animal
    _bar_duration : str
        FIXME a redundant field because constant

    Methods
    -------
    transform(NoteSequence):
        Extends the original note sequences with all subsequences that have a length of _max_duration
    """

    def __init__(self, max_duration: int, n_bars: int, name: str):
        super().__init__(name)
        self._max_duration = max_duration
        self._n_bars = n_bars
        self._bar_duration = (60 / QPM) * 4


    def transform(self, note_sequence):
        if not note_sequence.total_time or note_sequence.total_time <= self._max_duration:
            return [note_sequence]

        n_bars_seq = int(np.round(note_sequence.total_time) / self._bar_duration)
        n_windows = n_bars_seq - self._n_bars + 1
        new_seqs = [extract_subsequence(note_sequence, self._bar_duration * i, self._bar_duration * (i + self._n_bars))
                for i in range(n_windows)]
        for seq in new_seqs:
            seq.total_time = self._n_bars * self._bar_duration
        return new_seqs


class RepeatSequence(NoteSequencePipeline):
    """
    NoteSequencePipeline object repeats NoteSequences until a certain duration is reached

    Attributes
    ----------
    _min_duration : float
        repeat the NoteSequence until this duration
    _max_repeat : int
        the number of times we allow the sequence to repeat at most

    Methods
    -------
    transform(NoteSequence):
        Extends the original NoteSequence with a version that repeats until the allowed duration is reached
    """

    def __init__(self, min_duration, max_repeat=2, name=None):
        super().__init__(name)
        self._min_duration = min_duration
        self._max_repeat = max_repeat


    def transform(self, note_sequence):
        if not note_sequence.total_time or note_sequence.total_time >= self._min_duration:
            return [note_sequence]

        ns_length = note_sequence.total_time
        end_time = min(self._min_duration, self._max_repeat * ns_length)
        return [note_sequence, repeat_sequence_to_duration(note_sequence, end_time)]


class TensorValidator(Pipeline):
    """
    Pipeline object from Magenta filtering NoteSequences and allowing those that can be converted to tensors

    Attributes
    ----------
    _data_converter : magenta.models.musicvae.data.BaseNoteSequenceConverter
        indication on how we would want to convert NoteSequences to tensors

    Methods
    -------
    transform(NoteSequence):
        Filter out NoteSequences that are not tensor convertable with _data_converter
    """


    def __init__(self, type_, config=None, data_converter=None, name=None):
        """
        Parameters
        ----------
        type_ :
            type of pipeline output (most often NoteSequence)
        config : str, default=None
            key to model configuration you're about to use for your tensors, contains a data_converter attribute
        data_converter : magenta.models.musicvae.data.BaseNoteSequenceConverter, optional
            Sets the attribute without going through the model configuration
        """

        super(TensorValidator, self).__init__(type_, type_, name)
        if config is None and data_converter is None:
            raise Exception('Either config or data converter is not None')

        if config:
            self._data_converter = CONFIG_MAP[config].data_converter
        elif data_converter:
            self._data_converter = data_converter


    def transform(self, note_sequence):
        tensors = self._data_converter.to_tensors(note_sequence)
        if tensors.lengths:
            return [note_sequence]
        else:
            return []


def random_bar_choice(n_bars, size):
    """ Chooses a random subsequence of a song sequence in bars

    This is a helper function for the RandomSubSequenceGenerator

    Parameters
    ----------
    n_bars : int
        length in number of bars of the sequence
    size : int
        length in number of bars of the subsequence you want to randomly extract

    Returns
    -------
    tuple (start, end) in bars
    """
    if n_bars <= size:
        return None
    else:
        max_start_bar = n_bars - size
        start_bar = np.random.randint(max_start_bar)
        return start_bar, start_bar + size


class RandomSubSequenceGenerator(NoteSequencePipeline):
    """
    NoteSequencePipeline object repeats NoteSequences until a certain duration is reached

    Attributes
    ----------
    _n_splits : int
        Amount of random subsequences to generate from notesequence
    _sizes : list of int
        sizes (in bars) of subsequences

    Methods
    -------
    transform(NoteSequence):
        Augment the original NoteSequence with randomly extracted subsequences
    """

    def __init__(self, n_splits=20, sizes=[4, 8, 12], name='randy'):
        super().__init__(name)
        self._n_splits = n_splits
        self._sizes = sizes
        self._bar_duration = (60 / QPM) * 4


    def transform(self, note_sequence):
        if not note_sequence.total_time:
            return [note_sequence]

        n_bars_seq = int(np.round(note_sequence.total_time) / self._bar_duration)

        generated = [note_sequence]
        for idx in range(self._n_splits):
            rand_size = np.random.choice(self._sizes)
            if rand_size >= n_bars_seq:
                continue
            i1, i2 = random_bar_choice(n_bars_seq, rand_size)
            seq = extract_subsequence(note_sequence, self._bar_duration * i1, self._bar_duration * i2)
            generated.append(seq)

        return generated


class KeyCondioner(NoteSequencePipeline):
    """
    NoteSequencePipeline that modulates the NoteSequence to a certain key.

    Assumes a trio as input.

    Attributes
    ----------
    _key : int \in [0, 12[
        key represented by an integer. C (do) is 0.

    Methods
    -------
    transform(NoteSequence):
        modulates the NoteSequence to _key. Assumes as trio as input
    """

    def __init__(self, key=0, name='conditioner'):
        super().__init__(name)
        self._key = key


    def transform(self, note_sequence):
        quant = quantize_note_sequence(note_sequence, steps_per_quarter=4)

        melody = Melody()
        melody.from_quantized_sequence(quant, instrument=0, pad_end=True, ignore_polyphonic_notes=True)
        melody.squash(min_note=48, max_note=72, transpose_to_key=self._key)

        bass = Melody()
        bass.from_quantized_sequence(quant, instrument=1, pad_end=True, ignore_polyphonic_notes=True)
        bass.squash(min_note=36, max_note=55, transpose_to_key=self._key)

        drums = DrumTrack()
        drums.from_quantized_sequence(quant)
        # FIXME don't know what this does but looked cool
        drums.increase_resolution(2)

        # easiest is to convert to PrettyMidi and then back
        pms = [
            note_sequence_to_pretty_midi(melody.to_sequence(instrument=0, program=0)),
            note_sequence_to_pretty_midi(bass.to_sequence(instrument=1, program=33)),
            note_sequence_to_pretty_midi(drums.to_sequence()),
        ]

        pm = PrettyMIDI()
        pm.instruments = reduce(lambda x, y: x + y, [m.instruments for m in pms])

        return [midi_to_note_sequence(pm)]
