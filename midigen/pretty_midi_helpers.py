
import numpy as np
import pretty_midi
import librosa.display
import tempfile
from pretty_midi import PrettyMIDI


def pitch_to_section(pitch):
    return pretty_midi.note_number_to_name(pitch)[0]

def get_segment(notes, start_time, end_time):
    """Return notes within time span"""
    return [pretty_midi.Note(pitch=n.pitch,
                             start=n.start-start_time,
                             end=n.end-start_time,
                             velocity=n.velocity)
            for n in notes
            if n.start >= start_time and n.end < end_time]



def get_n_bars(pm, bar_duration):
    return int(np.round(pm.get_end_time() / bar_duration))


def plot_piano_roll(pm, start_pitch, end_pitch, fs=100):
    # Use librosa's specshow function for displaying the piano roll
    librosa.display.specshow(pm.get_piano_roll(fs)[start_pitch:end_pitch],
                             hop_length=1, sr=fs, x_axis='time', y_axis='cqt_note',
                             fmin=pretty_midi.note_number_to_hz(start_pitch))


def make_mini_midi(instr_track, outdir, prefix=None):
    midi = PrettyMIDI()
    if isinstance(instr_track, list):
        midi.instruments = instr_track
    else:
        midi.instruments = [instr_track]
    _, fp = tempfile.mkstemp(prefix=prefix, suffix='.mid', dir=outdir)
    midi.write(fp)
    print('Written to {}'.format(fp))