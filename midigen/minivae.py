import numpy as np
import joblib
import matplotlib.pyplot as plt
import argparse
import os
import sys
import json

import joblib
import glob

from keras.layers import Lambda, Input, Dense
from keras.models import Model
from keras.losses import mse, binary_crossentropy, kullback_leibler_divergence
import keras.optimizers
from keras import backend as K
from keras.models import model_from_json


def sampling(args):
    z_mean, z_log_var = args
    batch = K.shape(z_mean)[0]
    dim = K.int_shape(z_mean)[1]
    # by default, random_normal has mean = 0 and std = 1.0
    epsilon = K.random_normal(shape=(batch, dim))
    return z_mean + K.exp(0.5 * z_log_var) * epsilon


def add_loss(vae, beta=2):
    loss_z = 0.5 * mse(vae.get_layer('encoder_input'), vae.get_layer('encoder_input'))
    loss_kl = -0.5 * K.sum(1 + vae.get_layer('z_log_var')
                           - K.square(vae.get_layer('z_mean'))
                           - K.exp(vae.get_layer('z_log_var')))
    loss_vae = loss_z + beta * loss_kl
    vae.add_loss(loss_vae)


def build_minivae(x_dim, enc_size, z_dim, beta=2, optimizer=keras.optimizers.Adam, learning_rate=0.00005):
    inputs = Input(shape=(x_dim,), name='encoder_input')
    x = inputs
    for dim in enc_size:
        x = Dense(dim)(x)
    z_mean = Dense(z_dim, name='z_mean')(x)
    z_log_var = Dense(z_dim, name='z_log_var')(x)
    z = Lambda(sampling, output_shape=(z_dim,), name='z')([z_mean, z_log_var])
    encoder = Model(inputs, [z_mean, z_log_var, z], name='encoder')

    latent_inputs = Input(shape=(z_dim,), name='z_sampling')
    y = latent_inputs
    for dim in enc_size[::-1]:
        y = Dense(dim)(y)
    outputs = Dense(x_dim)(y)
    decoder = Model(latent_inputs, outputs, name='decoder')

    outputs = decoder(encoder(inputs)[2])
    minivae = Model(inputs, outputs, name='minivae')

    loss_z = 0.5 * mse(inputs, outputs)
    loss_kl = -0.5 * K.sum(1 + z_log_var - K.square(z_mean) - K.exp(z_log_var))
    loss_vae = loss_z + beta * loss_kl
    minivae.add_loss(loss_vae)

    minivae.compile(optimizer=optimizer(learning_rate=learning_rate))

    return minivae


def restore_model(js_pth, w_pth):
    js = open(js_pth, 'r').read()
    model = model_from_json(js)
    model.load_weights(w_pth)
    return model


def minivae_encode(vae, X):
    return vae.get_layer('encoder').predict(X)


def minivae_sample(vae, batch_size):
    z = np.random.randn(batch_size, vae.get_layer('decoder').input_shape[-1])
    return vae.get_layer('decoder').predict(z)


def minivae_groove(vae, X):
    z_mean, z_log_var, z = minivae_encode(vae, X)
    return vae.get_layer('decoder').predict(z)


def minivae_write(vae,  target_dir, name='MiniVAE', suffix='', incl_weights=False):
    model_name = "{model}.json".format(model=name)
    weight_name = "{name}-{suffix}.h5".format(name=name, suffix=suffix)

    target_js = os.path.join(target_dir, model_name)
    target_w = os.path.join(target_dir, weight_name)

    js = vae.to_json()
    with open(target_js, 'w') as f:
        f.write(js)
    if incl_weights:
        vae.save_weights(target_w)


def minivae_read(dirpth):
    js_pth = glob.glob(dirpth + '/*json')[0]
    w_pth = glob.glob(dirpth + '/*h5')[0]
    return restore_model(js_pth, w_pth)


def minivae_transfer(seq, minivae, vae, level=0.25, depth=20):
    int_level = int(depth * level)
    seq_transfer = vae.decode(minivae_sample(minivae, 1))[0]
    interpol = vae.interpolate(seq, seq_transfer, num_steps=depth+1)
    return interpol[int_level]





