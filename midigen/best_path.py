import numpy as np

def calc_dist(x, y):
    """Calculate the distance between two vectors."""
    return np.linalg.norm(x-y)

def calc_path_dist(path):
    """Calculate the total distance of a path."""
    length = 0
    for i in range(1, len(path)):
        length += calc_dist(path[i-1], path[i])
    return length

def calc_ttl_dist(combo, struct):
    """Given a combo and structure, calculate the total distance of the represented song."""
    song = get_song(combo, struct)
    return calc_path_dist(song)

def get_song(combo, struct):
    """
    Given the chosen combination and structure return the full song.

    Parameters
    ----------
    combo : int[]
        list of integers holding the choice of each of the song parts.
        For example if the sequence_sets were (verses, chorusses)
        then combo = [4, 5] means we chose the 5th verse and the 6th chorus.
    struct : (numpy.array[], int)()
        list of tuples containing a sequence_set and the index of that sequence set in the sequence_sets
        For example if the sequence_sets were (verses, chorusses)
        then struct can be for example ((verses, 0), (chorusses, 1), (verses, 0))

    Returns
    -------
    song : numpy.array[]
        the full song taking into account the combo and struct
        If the sequence_sets, combo and struct were as in the examples above,
        this returns [verses[4], chorusses[5], verses[4]]

    """
    return [i[0][combo[i[1]]] for i in struct]

def get_optimal_combo(best, cur, sequence_sets, struct):
    """
    Generate the optimal combo IE the song with the minimum distance between its parts.
    
    Parameters
    ----------
    best : int[]
        the best combo found so far
    cur : int[]
        the current combo being built and/or evaluated
    sequence_sets : np.array[]()
        tuple of lists of generated sequences
        Contains all generated sequence to be combined, for example (verses, chorusses)
        with verses and chorusses both lists of numpy arrays containing vectors for the generated sequences.
    struct : (numpy.array[], int)()
        tuple of tuples containing a sequence_set and the index of that sequence set in the sequence_sets
        For example if the sequence_sets were (verses, chorusses)
        then struct can be for example ((verses, 0), (chorusses, 1), (verses, 0))
    
    Returns
    -------
    combo : int[]
        list of integeres containing the 'optimal' choice for every sequence set
        For example if sequence_sets = (verses, chorusses), then if combo = [4, 5]
        that means we choose verses[4] and chorusses[5] for our song.
    """
    if sequence_sets == ():
        if calc_ttl_dist(cur, struct) < calc_ttl_dist(best, struct):
            return cur
        else:
            return best

    for seq in range(len(sequence_sets[0])):
        new = cur + [seq]
        best = get_optimal_combo(best, new, sequence_sets[1:], struct)

    return best

def get_dict(combo, sequence_sets):
    """Convert the chosen combination to a dictionary containing the type of sequence followed by the chosen vector.
    
    Parameters
    ----------
    combo : int[]
        list of integeres containing the 'optimal' choice for every sequence set
        For example if sequence_sets = [verses, chorusses], then if combo = [4, 5]
        that means we choose verses[4] and chorusses[5] for our song.
    sequence_sets : np.array[]()
        tuple of lists of generated sequences
        Contains all generated sequence to be combined, for example (verses, chorusses)
        with verses and chorusses both lists of numpy arrays containing vectors for the generated sequences.
    
    Returns
    -------
    my_dict : dict
        Dictionary containing as keys part_x : np.array
    """
    my_dict = {}
    for x in range(len(sequence_sets)):
        my_dict['part ' + str(x)] = sequence_sets[x][combo[x]]
    return my_dict
