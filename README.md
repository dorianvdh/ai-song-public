# Beatroots

AI song competition 2020

## installation

From the root in the repo

```
# install necessities for playing midi and soundfonts
apt-get update -qq && apt-get install -qq libfluidsynth1 fluid-soundfont-gm build-essential libasound2-dev libjack-dev

# python env
pip install pyfluidsynth
pip install -r requirements.txt

# get pretrained checkpoints (WARNING: >2GB each)
CHECKPOINT_DIR=trained/checkpoints
if [[ ! -d $CHECKPOINT_DIR ]]; then
  mkdir -p $CHECKPOINT_DIR
  gsutil cp gs://ai-song-contest-public/checkpoint_run1_eurovision.tar $CHECKPOINT_DIR
  gsutil cp gs://ai-song-contest-public/trio_16bar.tar $CHECKPOINT_DIR
  tar -xf $CHECKPOINT_DIR/checkpoint_run1_eurovision.tar -C $CHECKPOINT_DIR
fi
```
